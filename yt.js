const { google } = require('googleapis');
const path = require('path');
const fs = require('fs');
const express = require('express');
const opn = require('open');

class playlist {
    constructor() {
        this.youtube = google.youtube('v3');
        this.playlist = null;
        this.auth = null;
    };
    async init() {
        const keyfile = path.join(__dirname, 'oauth2.keys.json');
        const keys = JSON.parse(fs.readFileSync(keyfile));
        const client = new google.auth.OAuth2(
            keys.installed.client_id,
            keys.installed.client_secret,
            keys.installed.redirect_uris[1]
        );

        this.authorizeUrl = client.generateAuthUrl({
            access_type: 'offline',
            scope: ['https://www.googleapis.com/auth/youtube'],
        });
        const app = express();
        app.get('/oauth2callback', (req, res) => {
            const code = req.query.code;
            client.getToken(code, (err, tokens) => {
                if (err) {
                    console.error('Error getting oAuth tokens:');
                    throw err;
                }
                client.credentials = tokens;
                this.auth = tokens;
                this.makeList();
                res.send('Authentication successful! Please return to the console.');
                server.close();
            });
        });
        const server = app.listen(3000, () => {
            opn(this.authorizeUrl, { wait: true });
        });
    }
    async makeList() {
        while (!this.auth) {}
        try {
            var result = await this.youtube.playlists.insert({ part: 'snippet', requestBody: { snippet: { title: 'streamlist' } }, access_token: this.auth.access_token });
        } catch (e) { console.log(e) };
        this.playlist = result.data.id;
    }
    async addSong(term) {
        if (term.includes("/")) {
            var split = term.split("=");
            if (split[split.length - 1].length == 11) {
                await this.youtube.playlistItems.insert({ part: 'snippet', requestBody: { snippet: { playlistId: this.playlist, resourceId: { kind: 'youtube#video', videoId: split[split.length - 1] } } }, access_token: this.auth.access_token });
                return null;
            } else {
                return 'bad url: make sure you don\'t include anything after the video id'
            }
        }
        var search = await this.youtube.search.list({ part: 'snippet', q: term, access_token: this.auth.access_token });
        await this.youtube.playlistItems.insert({ part: 'snippet', requestBody: { snippet: { playlistId: this.playlist, resourceId: search.data.items[0].id } }, access_token: this.auth.access_token });
        return null;
    }

    
}
module.exports = playlist;