const pmi = require('pmi.js');
const yt = require('./yt');
const fs = require('fs');
const path = require('path');
const prompt = require('prompt-sync')();

class limiter {
    //so. this definitely isn't where I want this block
    constructor() {
        this.calls = [];
    }
    limitedSay(message) {
        this.clearCalls();
        if (this.calls.length >= 20) {
            return 'too many calls! try again later';
        }
        this.calls.push(Date.now());
        client.say('', message);
    }
    clearCalls() {
        const now = Date.now() //gives up a little precision to not get the time 20 times
        const curcalls = this.calls; //ughhhh this can't be the best way
        this.calls.forEach(
            function callback(call) {
                if (Date.now() - call >= 30000) {
                    curcalls.splice(curcalls.indexOf(call), 1);
                }
            });
        this.calls = curcalls; //look if you know how callbacks work please help me i'm sad

    }
}
/* Define configuration options */
const credpath = path.join(__dirname, 'token.txt');
if (!fs.existsSync(credpath)) {
    if (prompt('picarto credentials file not found. create one? [Y/n]') !== 'n') {
        const username = prompt('picarto username: ');
        const token = prompt('bot token (can be found through pmi.picarto.tv): ');
        console.log(username + " " + token);
        const payload = username + " " + token;
        fs.writeFileSync(credpath, payload);
    } else {
        throw ('you need a credentials file for the bot to work');
    }
}
const token = fs.readFileSync(path.join(__dirname, 'token.txt'), 'utf-8');
const creds = token.split(' ');
const opts = {
    identity: {
        username: creds[0],
        password: creds[1]
    }
};
/* Create a client with our options */
client = new pmi.client(opts);
//make the limiter
const rl = new limiter();
/* Register our event handlers */
client.on('message', onMessageHandler);
client.on('connected', onConnectedHandler);
client.on('join', onJoinHandler);
//set up youtube api stuff
var ytenable = prompt('enable youtube? [y/N]');
ytenable = ytenable == 'y';
if (ytenable) {
    var playlist = new yt;
    playlist.init();
}

//set up responses
var repath = path.join(__dirname, 'responses.txt');
if (!fs.existsSync(repath)) {
    fs.writeFileSync(repath, '{}');
}
var responses = fs.readFileSync(repath);
responses = JSON.parse(responses);
/* Connect to Picarto: */
client.connect();
/* Called every time a message comes in*/
function onMessageHandler(target, context, msg, self) {
    if (!context[0].s) { return; } //ignore bot messages
    commandName = msg.split(" ");
    /* actually do this a normal way */
    if (commandName[0].charAt(0) === "!") {
        var command = commandName[0].substring(1).toLowerCase();
        if (command in responses) {
            rl.limitedSay(responses[command]);
        } else {
            switch (command) {
                case 'sr':
                case 'songrequest':
                    if (ytenable) {
                        playlist.addSong((commandName.slice(1).join(" ")));
                    } else {
                        console.log('youtube not enabled! restart the bot to use song requests');
                    }
                    break;
                case 'register':
                    addResponse(commandName[1], commandName.slice(2).join(" ")) //it goes !register command output
                    break;
                case 'commands':
                    listCommands();
            }
        }
    }
}


/* Called every time the bot connects to Picarto chat */
function onConnectedHandler(addr, port) {
    console.log("* Connected to " + addr + ":" + port);
}

function onJoinHandler(user) {
    rl.limitedSay('Welcome, ' + user + '!');
}
function addResponse(call, response) {
    responses[call] = response;
    var out = JSON.stringify(responses);
    fs.writeFileSync('responses.txt', out);
}
function listCommands() {
    //idk how to do these other than hardcoding them :(
    var commands = ['songrequest, register, commands'];
    for (const [key, value] of Object.entries(responses)) {
        commands.push(key);
    }
    rl.limitedSay('Current commands: ' + commands.join(', '));
}
